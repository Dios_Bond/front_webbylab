import React from 'react';

import '../css/style.css';

function Nav() {
  return (
    
      <div>
          <ul className='navbar'>
              <li><a href='/add'>Add Films / Import from txt file</a></li>
              <li><a href='/search'>Search</a></li>
          </ul>
      </div>
    
  );
}

export default Nav;