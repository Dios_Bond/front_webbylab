import React from 'react';
import conf from '../config';
import NavHome from '../view/NavHome';
const urlSearch = conf.serverconnect.link + ':' + conf.serverconnect.port + '/search';

function createLi(data){

    if (data.data == undefined ){
        return <div>Not data</div>
    }
    else {
        let map = data.data.map(el => {
            return <li key={el.id}><a href={'/films/'+el.id}>{el.name}</a></li> 
        })
        return map
    }
};


class SearchFilms extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            searchReq: {
                name: [],
                // "stars": []
            },
            dataSearch: []
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleInputFunc = this.handleInputFunc.bind(this);

    }
        handleFormSubmit = async (event) => {
            event.preventDefault();
            let searchReq = this.state.searchReq;
            
            
            let resData = await fetch(urlSearch, {
                method: "POST",
                body: JSON.stringify(searchReq),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });
            const data = await resData.json();
            console.log(data);

            this.setState(prevState => ({dataSearch: 
                {...prevState.dataSearch, data}})
                ,() => console.log('Renew data Search', this.state.dataSearch))

        }

        handleInputFunc (event){
            let val = event.target.value;
            let name = event.target.name;
            this.setState(prevState => ({searchReq: 
                {...prevState.searchReq, [name]: [val]}})
                ,() => console.log(this.state.searchReq))
        }

        render(){
            return (
      
                <div>
                    <NavHome />
                    <h1>Search Film by name or/and stars</h1>
                    <form className = 'formView' onSubmit = {this.handleFormSubmit}>
                        <label>
                            Input Name Films:
                            <input name = 'name' type = 'text' onChange = {this.handleInputFunc}></input> 
                            {/*  //{this.state.film.title} */}
                        </label>
                        <br />
                        <label>
                            Input Stars:
                            {/* <input name = 'stars' type = 'text' onChange = {this.handleInputFunc}></input> */}
                        </label>
                        <br />
                        <input type = 'submit' value = 'Search'></input>
                    </form>
                          
                    <ul>  
                        {createLi(this.state.dataSearch)}
                    </ul>
                </div>
              
            );

        }
        
    }
    


export default SearchFilms;