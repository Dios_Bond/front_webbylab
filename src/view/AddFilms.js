import React from 'react';
import conf from '../config';
import NavHome from '../view/NavHome';
const urlFilms = conf.serverconnect.link + ":" + conf.serverconnect.port + "/films/";
const urlImport = conf.serverconnect.link + ':' + conf.serverconnect.port + '/films-import';

class AddFilms extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            film: {
                title: "",
                release_year: "",
                format: "",
                stars: ""
            },
            formatOptions: ['VHS', 'DVD', 'Blu-Ray'],
            file: ''
        }
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleInputFunc = this.handleInputFunc.bind(this);
        this.handleFormSendFile = this.handleFormSendFile.bind(this);
        this.habndleFileFunc = this.habndleFileFunc.bind(this);
    }
        handleFormSubmit (event){
            event.preventDefault();
            let newFilm = this.state.film;
            
            fetch(urlFilms, {
                method: "POST",
                body: JSON.stringify([newFilm]),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => {
                console.log(res.status)
            })

        }

        handleFormSendFile (event){
            event.preventDefault();
            //console.log(event)
            let file = this.state.file;

            fetch(urlImport, {
                method: "POST",
                body: file,
                headers: {
                    'Accept': 'txt',
                    'Content-Type': 'multipart/form-data'
                }
            }).then(res => {
                console.log(res.status)
            })
        }

        handleInputFunc (event){
            let val = event.target.value;
            let name = event.target.name;
            this.setState(prevState => ({film: 
                {...prevState.film, [name]: val}})
                ,() => console.log(this.state.film))
        }

        habndleFileFunc (event){
            let file = event.target.files[0];
            console.log(file);
            this.setState({file: file}) 
        }

        render(){
            return (
      
                <div>
                    <h1>Add Film</h1>
                    <form className = 'formView' onSubmit = {this.handleFormSubmit}>
                        <label>
                            Name:
                            <input name = 'title' type = 'text' onChange = {this.handleInputFunc}></input> 
                            {/*  //{this.state.film.title} */}
                        </label>
                        <br />
                        <label>
                            Release year:
                            <input name = 'release_year' type = 'date' onChange = {this.handleInputFunc}></input>                
                        </label>
                        <br />
                        <label>
                            Format:
                            <select 
                                name = 'format' 
                                value = {this.state.film.format} 
                                onChange = {this.handleInputFunc}
                                >
                                    <option value = '' disabled></option>
                                {this.state.formatOptions.map(opt => {
                                    return(
                                        <option
                                        key = {opt}
                                        value = {opt}
                                        label = {opt}
                                        >{opt}
                                        </option>
                                    )
                                })} 

                                </select>
                        </label>
                        <br />
                        <label>
                            Stars:
                            <input name = 'stars' type = 'text' onChange = {this.handleInputFunc}></input>
                        </label>
                        <br />
                        <input type = 'submit' value = 'Submit'></input>
                    </form>

                    <p></p>
                    <h1>Import Film from file</h1>
                    <form className = 'formView' onSubmit = {this.handleFormSendFile}>
                        <label>
                            Select file for import:
                            <input name = 'file' type = 'file' onChange = {this.habndleFileFunc}></input> 
                            {/*  //{this.state.film.title} */}
                        </label>
                        <br/>
                        <input type = 'submit' value = 'Send File'></input>
                    </form>
                    <p></p>
                          <NavHome />
                </div>
              
            );

        }
        
    }
    


export default AddFilms;