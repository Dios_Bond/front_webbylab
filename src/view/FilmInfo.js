import React, { Children } from 'react';
import conf from '../config';
import NavMenu from './NavHome';
const urlFilms = conf.serverconnect.link + ":" + conf.serverconnect.port + "/films/";

function removeFunc(id) {
    fetch(urlFilms + id, {
        method: 'DELETE'
    });
}

function CreateInfo (data) {
    let map = [];
    data.map(el => {
        let i = 0;
        for (let key in el){ 
            map.push(<li key={i++}>{key}:{el[key]}</li>)  
        }
    });
       return map
}

class BlockInfo extends React.Component {
    //function Stat() {
        constructor(props) {
          super(props);
          this.state = {id: this.props.match.params.id, data: [] };
        }
      
        componentDidMount = async() => {
          const response = await fetch(urlFilms + this.state.id);
          const data = await response.json();
          this.setState({data})
        }
    
        render(){
            //console.log(this.state.data)
            //const data = Array.from(this.state.data);
            let data = this.state.data;
            console.log('data film info ', data);
    
      return (
            <div className='listView'>
                <ul>
                {CreateInfo(data)}
                </ul>
                <br/>
                <button onClick={removeFunc(this.state.id)}>Remove</button>
                <br/>
                <NavMenu/>
            </div>
  
        )
    }}

export default BlockInfo;