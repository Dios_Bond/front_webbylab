import React from 'react';
import Nav from './Nav';

import conf from '../config';
const urlFilms = conf.serverconnect.link + ":" + conf.serverconnect.port + "/films/";

function createLi(data){

    let map = data.map(el => {
        
            //return <li className='activpage' key={page}><a href={'/stat/'+page}>{page}</a></li>
            console.log('element', el)
            return <li key={el.id}><a href={'/films/'+el.id}>{el.name}</a></li>
       
    })
   
    return map

};

// function BlockList(data) {
//     return (
      
//         <div className='pageNumber'>
//             <ul>  
//                 {createLi(data)}
//             </ul>
//         </div>
      
//     )
//   };


  class BlockList extends React.Component {
    //function Stat() {
        constructor(props) {
          super(props);
          //this.state ={ page: this.props.match.params.page, data: [] };
          this.state = {data: []};
        }
      
        componentDidMount = async() => {
          const response = await fetch(urlFilms);
          const data = await response.json();
          this.setState({data})
        }
    
        render(){
            //console.log(this.state.data)
            //const data = Array.from(this.state.data);
            let data = this.state.data;
            console.log('data', data);
    
      return (
            <React.Fragment>
                <Nav />
                <div className='listView'>
                <ul>  
                    {createLi(data)}
                </ul>
                </div>
            </React.Fragment>
  
        )
    }}

  export default BlockList;