import React from 'react';
//import logo from './logo.svg';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';

import Head from './view/Header';
import MainList from './view/List'; 
import FilmInfo from './view/FilmInfo';
import AddFilm from './view/AddFilms';
import Search from './view/Search';



function App() {
  return (
    <div className="App">
      <Head />
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <Router>
        <Switch>
            <Route exact path = '/' component = {MainList}></Route>
            <Route exact path = '/films/:id' component = {FilmInfo}></Route>
            <Route exact path = '/add/' component = {AddFilm}></Route>
            <Route exact path = '/search/' component = {Search}></Route>
        </Switch>
      </Router>
      {/* <MainList /> */}
    </div>
  );
}

export default App;
